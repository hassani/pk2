program sample10_userfunc2
!---------------------------------------------------------------------------------------------
!  In this example the user give:
!   - the litteral expression of a real function of two real variables (x,y)
!   - any constants used in this expression
!   - a set of values for x and y where the function will be evaluated
!  Same as sample10_userfunc1 but using a vectorial form (more efficient than a do-loop)
!---------------------------------------------------------------------------------------------
   use calmat_m
   implicit none
   logical, parameter              :: Yes = .true., No = .false.
   character(len=100)              :: userConstants="", userFunction=""
   real     ( Rkind ), allocatable :: X1(:), X2(:), X(:,:), Y(:,:), F(:,:)
   integer  ( Ikind )              :: nx, ny, u, err, i, j, k
!---------------------------------------------------------------------------------------------

!
!- 1) Read the user data:
!  ======================

   print '(/,a)','Reading the user data in userfunc2.input'

   open(newunit=u, file='userfunc2.input', status='old', action='read', iostat=err) 
   if ( err /= 0 ) stop 'Unable to open the input file "userfunc2.input"'
   
   ! read the user function: we assume this is a real expression of the two real variables x 
   ! and y and that the result is noted f (a valid expression is for example: f = 3*x + 2*y)
   read(u,'(a)',iostat=err) userFunction
   
   ! if the user function involves some constants define them (otherwise set an empty line), 
   ! for example, if f = a*x + by, define a and b on the same line, e.g.: a = 3 ; b = 2 
   if ( err == 0 ) read(u,'(a)',iostat=err) userConstants
   
   ! read the number of x values and y values:
   if ( err == 0 ) read(u,*,iostat=err) nx, ny
   if ( err == 0 ) allocate(X1(nx), X2(ny), X(nx*ny,1), Y(nx*ny,1))
   
   ! read these values:
   if ( err == 0 ) read(u,*,iostat=err) (X1(i),i=1,nx)
   if ( err == 0 ) read(u,*,iostat=err) (X2(i),i=1,ny)
   
   if ( err /= 0 ) stop 'Error occured during the read'
   close(u)
   
   ! form the grid points (X1(i),X2(j)):
   do j = 1, ny
      do i = 1, nx
         k = (j-1)*nx + i ; X(k,1) = X1(i) ; Y(k,1) = X2(j)
      end do
   end do
   
   open(newunit=u, file='userfunc2.output', status='replace', action='write', iostat=err) 
   if ( err /= 0 ) stop 'Unable to open the output file "userfunc2.output"'
! 
!- 2) Initialize the user constants (if any):
!  ==========================================

   call calmat ( warning = Yes, welcome = No, dispRes = No, exprIn = trim(userConstants) )   
!
!- 3) Create and set two new variables 'x' and 'y' and evaluate the user function:
!  ===============================================================================

   call calmat_moveAlloc ( from = X, to = 'x' ) 
   call calmat_moveAlloc ( from = Y, to = 'y' )

   print '(/,a,i0,a,i0,a,i0,a)','Evaluate "'//trim(userFunction)//'" at ',nx,' x ',ny, &
                                ' = ',nx*ny,' points'

   call calmat ( exprIn = trim(userFunction) )
!
!- 4) Get and write the results:
!  =============================

   call calmat_moveAlloc ( from = 'x', to = X )
   call calmat_moveAlloc ( from = 'y', to = Y )
   call calmat_moveAlloc ( from = 'f', to = F )

   do i = 1, size(X)
      write(u,*)X(i,1),Y(i,1),F(i,1)
   end do
   close(u)

   print '(/,a)','Done. The result is printed in userfunc2.output'
   print '(a,/)','(You can draw the result with gnuplot for example)'
   
end program sample10_userfunc2
