program sample8_userfunc1
!-----------------------------------------------------------------------------------------------
!  In this example the user give:
!   - the litteral expression of a real function of two real variables (x,y)
!   - any constants used in this expression
!   - a set of values for x and y where the function will be evaluated
!  We use here a do-loop (see sample10_userfunc2.f90 for a more efficient vectorial version)
!-----------------------------------------------------------------------------------------------
   use calmat_m
   implicit none
   logical, parameter              :: Yes = .true., No = .false.
   character(len=100)              :: userConstants="", userFunction=""
   real     ( Rkind ), allocatable :: X(:), Y(:), F
   integer  ( Ikind )              :: nx, ny, u, err, i, j, flowId
   type     ( ind_t )              :: indx, indy, indf
!-----------------------------------------------------------------------------------------------

!
!- 1) Read the user data:
!  ======================
   
   print '(/,a)','Reading the user data in userfunc1.input'
   
   open(newunit=u, file='userfunc1.input', status='old', action='read', iostat=err) 
   if ( err /= 0 ) stop 'Unable to open the input file "userfunc1.input"'
   
   ! read the user function: we assume this is a real expression of the two real variables x 
   ! and y and that the result is noted f (a valid expression is for example: f = 3*x + 2*y)
   read(u,'(a)',iostat=err) userFunction
   
   ! if the user function involves some constants define them (otherwise set an empty line), 
   ! for example, if f = a*x + by, define a and b on the same line, e.g.: a = 3 ; b = 2 
   if ( err == 0 ) read(u,'(a)',iostat=err) userConstants
   
   ! read the number of x values and y values:
   if ( err == 0 ) read(u,*,iostat=err) nx, ny
   if ( err == 0 ) allocate(X(nx), Y(ny))
   
   ! read these values:
   if ( err == 0 ) read(u,*,iostat=err) (X(i),i=1,nx)
   if ( err == 0 ) read(u,*,iostat=err) (Y(i),i=1,ny)
   
   if ( err /= 0 ) stop 'Error occured during the read'
   close(u)
   
   open(newunit=u, file='userfunc1.output', status='replace', action='write', iostat=err) 
   if ( err /= 0 ) stop 'Unable to open the output file "userfunc1.output"'
! 
!- 2) Initialize the calmat variables x, y, f and the user constants (if any):
!  ===========================================================================

   call calmat ( warning = Yes, welcome = No, dispRes = No,               &
                 exprIn = trim(userConstants) // '; x=0.0; y=0.0; f=0.0;' )
   
   ! and get the indexes of the calmat variables x, y and f
   call calmat_inquire ( 'x', indx )
   call calmat_inquire ( 'y', indy )
   call calmat_inquire ( 'f', indf )
!
!- 3) Parse the user function:
!  ===========================

   print '(/,a,i0,a,i0,a,i0,a)','Evaluate "'//trim(userFunction)//'" at ',nx,' x ',ny, &
                                ' = ',nx*ny,' points'
      
   call calmat ( exprIn = trim(userFunction), parseOnly = flowId )
!
!- 4) Evaluate the user function at the sample points (x(i),y(j)):
!  ===============================================================

   do j = 1, ny
      ! set 'y' to Y(j):
      call calmat_copy ( from = Y(j), to = indy )
      do i = 1, nx
         ! set 'x' to X(i):
         call calmat_copy ( from = X(i), to = indx )
         ! evaluate the user function:
         call calmat ( evaluate = flowId )
         ! get the value of 'f':
         call calmat_copy ( from = indf, to = F )
         ! write the result:
         write(u,*)X(i),Y(j),F
      end do
   end do
   close(u)
   
   print '(/,a)','Done. The result is printed in userfunc1.output'
   print '(a,/)','(You can draw the result with gnuplot for example)'
   
end program sample8_userfunc1
