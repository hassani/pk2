program sample9_userfunc1Bis
!---------------------------------------------------------------------------------------------
!  (same as sample8_userfunc1.f90 but the name of the function and the variables are only
!   known at runtime)
!
!  In this example the user give:
!   - the litteral expression of a real function of two real variables 
!   - the name of the function and the names of the variables
!   - any constants used in this expression
!   - a set of values for the two variables where the function will be evaluated
!---------------------------------------------------------------------------------------------
   use calmat_m
   implicit none
   logical, parameter              :: Yes = .true., No = .false.
   integer, parameter              :: Nvar = 2
   character(len=100)              :: userConstants="", userFuncExpr="", userFuncName="", &
                                      userVarNames(2)
   character(len=:  ), allocatable :: expr
   real     ( Rkind ), allocatable :: X(:), Y(:), F(:)
   integer  ( Ikind )              :: u, err, i, j, ivar, flowId, nsample(nVar)
   type     ( ind_t )              :: indFunc, indVar(nVar)
!---------------------------------------------------------------------------------------------

!
!- 1) Read the user data:
!  ======================

   print '(/,a)','Reading the user data in userfunc1Bis.input'

   open(newunit=u, file='userfunc1Bis.input', status='old', action='read', iostat=err) 
   if ( err /= 0 ) stop 'Unable to open the input file "userfunc1Bis.input"'
   
   ! read the user function expression. We assume this is a real expression of two real 
   ! variables, whatever their names. (for example: "3*Myx + 2*Y" with Myx and Y):
   read(u,'(a)',iostat=err) userFuncExpr
   
   ! read the user function name (for example: f) and the names of the involved variables:
   if ( err == 0 ) read(u,*,iostat=err) userFuncName
   if ( err == 0 ) read(u,*,iostat=err) (userVarNames(ivar),ivar=1,Nvar)

   ! if the user expression involves some constants define them (otherwise set an empty line), 
   ! e.g., for a*x + by, define a and b on the same line, e.g.: a = 3 ; b = 2 
   if ( err == 0 ) read(u,'(a)',iostat=err) userConstants

   ! read the number of values for each variable where the function is to be evaluated:
   if ( err == 0 ) read(u,*,iostat=err) (nsample(ivar),ivar=1,Nvar)
   if ( err == 0 ) allocate(X(nsample(1)),Y(nsample(2)))
   
   ! read these values:
   if ( err == 0 ) read(u,*,iostat=err) (X(i),i=1,nsample(1))
   if ( err == 0 ) read(u,*,iostat=err) (Y(i),i=1,nsample(2))
   
   if ( err /= 0 ) stop 'Error occured during the read'
   close(u)
   
   open(newunit=u, file='userfunc1Bis.output', status='replace', action='write', iostat=err) 
   if ( err /= 0 ) stop 'Unable to open the output file "userfunc1Bis.output"'
! 
!- 2) Initialize the user constants (if any), the user function and the user variables:
!  ====================================================================================

   expr = trim(userFuncName) // '=[];' 
   do ivar = 1, Nvar
      expr = expr // ';'//trim(userVarNames(ivar))//'=[];'
   end do
   expr = expr // trim(userConstants) // ';'
    
   call calmat ( warning = Yes, welcome = No, dispRes = No, exprIn = expr )   

   ! and get the indexes of the user variables and user function:
   do ivar = 1, Nvar
      call calmat_inquire ( trim(userVarNames(ivar)), indVar(ivar) )
   end do
   call calmat_inquire ( trim(userFuncName), indFunc)
!
!- 3) Parse the user function:
!  ===========================

   print '(/,a)','Parsing "'//trim(userFuncName)//' = '//trim(userFuncExpr)//'"'

   call calmat ( exprIn = trim(userFuncName)//'='//trim(userFuncExpr), parseOnly = flowId )
!
!- 4) Evaluate the user function at the sample points (X(i),Y(j)):
!  ===============================================================

   print '(/,a,i0,a,i0,a,i0,a)','Evaluate '//trim(userFuncName)//' at ',nsample(1), &
                                ' x ',nsample(2),' = ',nsample(1)*nsample(2),' points'

   do j = 1, nsample(2)
      call calmat_copy ( from = Y(j), to = indVar(2) )
      do i = 1, nsample(1)
         call calmat_copy ( from = X(i), to = indVar(1) )
         ! evaluate the user function:
         call calmat ( evaluate = flowId )
         ! get the value of 'f':
         call calmat_copy ( from = indFunc, to = F )
         ! write the result:
         write(u,*)X(i),Y(j),F
      end do
   end do
   close(u)

   print '(/,a)','Done. The result is printed in userfunc1Bis.output'
   print '(a,/)','(You can draw the result with gnuplot for example)'

   
end program sample9_userfunc1Bis
