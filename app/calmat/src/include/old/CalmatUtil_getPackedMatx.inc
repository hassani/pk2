   integer(Ikind),              intent(in    ) :: from 
   type   (err_t),              intent(in out) :: stat
   logical       , optional,    intent(in    ) :: movealloc

   character(len=:), allocatable :: name 
   logical                       :: movealloc_

   if ( from > 0 .and. from <= size(G_vars) ) then 
      if ( present(movealloc) ) then
         movealloc_ = movealloc
      else
         movealloc_ = .false.
      end if
      call G_vars(from)%GetMatPacked ( to, stat ) ; if_error_trace_and_RETURN ( stat, HERE )
      if ( movealloc_ ) then
         call move_alloc ( from = G_vars(from)%name, to = name ) 
         call G_vars(from)%pk2_t%destroy()
         call move_alloc ( from = name, to = G_vars(from)%name ) 
      end if
   end if
