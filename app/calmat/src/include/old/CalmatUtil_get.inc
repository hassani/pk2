   character(len=*),              optional, intent(in    ) :: varname
   integer  (Ikind),              optional, intent(in    ) :: varId
   type     (err_t),                        intent(in out) :: stat
   
   integer  (Ikind)              :: i
   character(len=:), allocatable :: name
   
   if ( .not. present(varId) ) then
      call CalmatUtil_FindVar ( varName = varName, varId = i )
   else
      i = varId
   end if   
   
   if ( present(pointer) ) then
      select rank(pointer)
         rank(0)
            call G_vars(i)%pointer ( pointer, stat )
         rank(1)
            call G_vars(i)%pointer ( pointer, stat )
         rank(2)
            call G_vars(i)%pointer ( pointer, stat )
         rank default
            stat =  err_t ( stat = G_UERROR, where = HERE, &
                            msg = 'unexpected rank'//util_intToChar(rank(pointer)) )
      end select
   else if ( present(copy) ) then
      select rank(copy)
         rank(0)
            call G_vars(i)%pointer ( pv11, stat )
            copy = pv11
         rank(1)
            call G_vars(i)%GetMatPacked ( copy, stat )
         rank(2)
            call G_vars(i)%getmat ( copy, stat )
         rank default
            stat =  err_t ( stat = G_UERROR, where = HERE, &
                            msg = 'unexpected rank'//util_intToChar(rank(copy)) )
            
      end select
   else if ( present(movealloc) ) then
      name = G_vars(i)%name
      select rank(movealloc)
         rank(0)
            call G_vars(i)%pointer ( pv11, stat )
            movealloc = pv11
            call G_vars(i)%pk2_t%Destroy()
         rank(1)
            call G_vars(i)%GetMatPacked ( movealloc, stat )      
            call G_vars(i)%pk2_t%Destroy()
         rank(2)
            call pk2_movealloc ( from = G_vars(i), to = movealloc, stat = stat )
      end select
      G_vars(i)%name = name

   else
      stat =  err_t ( stat = G_IERROR, where = HERE, &
                            msg = 'Missing "pointer", "copy" or "movealloc"' )
   end if

   if_error_trace_and_RETURN ( stat, HERE )
