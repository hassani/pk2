   character(len=*), intent(in    ) :: to
   type     (err_t), intent(in out) :: stat

   integer  (Ikind) :: i
   
   call CalmatUtil_FindVar ( varName = to, varId = i )
   
   if ( i > 0 .and. i <= size(G_vars) ) then
      select rank ( from )
         rank ( 0 )
            call pk2_assign ( lhs=G_vars(i), rhs=from, stat=stat )
         rank ( 1 )
            call pk2_assign ( lhs=G_vars(i), rhs=from, stat=stat )
         rank ( 2 )
            call pk2_assign ( lhs=G_vars(i), rhs=from, stat=stat )
         rank default
            stat =  err_t ( stat = G_UERROR, where = HERE, &
                            msg = 'unexpected rank '//util_intToChar(rank(from)) )
            return
      end select
      if_error_trace_and_RETURN ( stat, HERE )
   end if
