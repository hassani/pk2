
   if (present(name)) then
      tmp = pk2_t ( v, name )
      call pk2_movealloc ( from = tmp, to = res%v, movename = .true. )
   else
      tmp = pk2_t ( v )
      call pk2_movealloc ( from = tmp, to = res%v, movename = .false. )
   end if
   
   if (present(status)  ) res%status   = status
   if (present(firstIns)) res%firstIns = firstIns
   if (present(lastIns) ) res%lastIns  = lastIns
   if (present(objId)   ) res%objId    = objId
   if (present(cmpObjId)) res%cmpObjId = cmpObjId