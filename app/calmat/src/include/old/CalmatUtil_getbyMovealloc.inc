   character(len=*), optional, intent(in    ) :: varname
   integer  (Ikind), optional, intent(in    ) :: varId
   type     (err_t),           intent(in out) :: stat
   
   integer  (Ikind)              :: i
   character(len=:), allocatable :: name
   
   if ( .not. present(varId) ) then
      call CalmatUtil_FindVar ( varName = varName, varId = i )
   else
      i = varId
   end if   
   
   name = G_vars(i)%name
   select rank(to)
      rank(0)
         call G_vars(i)%pointer ( pv11, stat )
         if ( .not. allocated(to) ) allocate(to)
         to = pv11
         call G_vars(i)%pk2_t%Destroy()
      rank(1)
         call G_vars(i)%GetMatPacked ( to, stat )      
         call G_vars(i)%pk2_t%Destroy()
      rank(2)
         call pk2_movealloc ( from = G_vars(i), to = to, stat = stat )
   end select
   G_vars(i)%name = name

   if_error_trace_and_RETURN ( stat, HERE )
