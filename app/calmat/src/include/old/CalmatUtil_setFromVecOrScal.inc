   integer(Ikind),           intent(in    ) :: to
   type   (err_t),           intent(in out) :: stat
   
   if ( to > 0 .and. to <= size(G_vars) ) then
      call pk2_assign ( lhs = G_vars(to), rhs = from, stat = stat )
      if_error_trace_and_RETURN ( stat, HERE )
   end if
