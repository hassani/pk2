   integer  (Ikind), intent(in    ) :: from
   type     (err_t), intent(in out) :: stat
         
   call G_vars(from)%pointer ( to, stat )
  
   if_error_trace_and_RETURN ( stat, HERE )