   character(len=*), optional, intent(in    ) :: varname
   integer  (Ikind), optional, intent(in    ) :: varId
   type     (err_t),           intent(in out) :: stat

   integer  (Ikind) :: i
   
   if ( .not. present(varId) ) then
      call CalmatUtil_FindVar ( varName = varName, varId = i )
   else
      i = varId
   end if
   
   if ( i > 0 .and. i <= size(G_vars) ) then
      select rank ( rhs )
         rank ( 0 )
            call pk2_assign ( lhs=G_vars(i), rhs=rhs, stat=stat )
     G_vars(i) = rhs
         rank ( 1 )
            call pk2_assign ( lhs=G_vars(i), rhs=rhs, stat=stat )
         rank ( 2 )
            call pk2_assign ( lhs=G_vars(i), rhs=rhs, stat=stat )
         rank default
            stat =  err_t ( stat = G_UERROR, where = HERE, &
                            msg = 'unexpected rank'//util_intToChar(rank(rhs)) )
            return
      end select
      if_error_trace_and_RETURN ( stat, HERE )
   end if
