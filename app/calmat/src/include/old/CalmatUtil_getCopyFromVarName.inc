   character(len=*), intent(in    ) :: from
   type     (err_t), intent(in out) :: stat
   
   integer  (Ikind)              :: i
   character(len=:), allocatable :: name
   
   call CalmatUtil_FindVar ( varName = from, varId = i )
   
   select rank(to)
      rank(0)
         call G_vars(i)%pointer ( pv11, stat )
         to = pv11
      rank(1)
         call G_vars(i)%GetMatPacked ( to, stat )
      rank(2)
         call G_vars(i)%getmat ( to, stat )
      rank default
         stat =  err_t ( stat = G_UERROR, where = HERE, &
                         msg = 'unexpected rank'//util_intToChar(rank(to)) )
         return
   end select

   if_error_trace_and_RETURN ( stat, HERE )