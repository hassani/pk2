   integer(Ikind),           intent(in    ) :: to
   type   (err_t),           intent(in out) :: stat
   logical       , optional, intent(in    ) :: movealloc

   logical                       :: movealloc_
   character(len=:), allocatable :: name
      
   if ( to > 0 .and. to <= size(G_vars) ) then
   
      if ( present(movealloc) ) then
         movealloc_ = movealloc
      else
         movealloc_ = .false.
      end if
      
      if ( movealloc_ ) then
         call pk2_movealloc ( from = from, to = G_vars(to), stat = stat )
      else
         if ( allocated(from) )  then
            call pk2_assign ( lhs = G_vars(to), rhs = from, stat = stat )
         else
            call move_alloc ( from = G_vars(to)%name, to = name )
            call G_vars(to)%pk2_t%Destroy()
            call move_alloc ( from = name, to = G_vars(to)%name )
         end if
      end if
      
      if_error_trace_and_RETURN ( stat, HERE )
   
   end if
