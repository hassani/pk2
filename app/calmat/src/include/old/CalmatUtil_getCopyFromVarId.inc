   integer  (Ikind), intent(in    ) :: from
   type     (err_t), intent(in out) :: stat
   
   character(len=:), allocatable :: name
   
   select rank(to)
      rank(0)
         call G_vars(from)%pointer ( pv11, stat )
         to = pv11
      rank(1)
         call G_vars(from)%GetMatPacked ( to, stat )
      rank(2)
         call G_vars(from)%getmat ( to, stat )
      rank default
         stat =  err_t ( stat = G_UERROR, where = HERE, &
                         msg = 'unexpected rank'//util_intToChar(rank(to)) )
         return
   end select

   if_error_trace_and_RETURN ( stat, HERE )