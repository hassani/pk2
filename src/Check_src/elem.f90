implicit none
integer :: expo(2,2), base(2,2), res(2,2)

expo = reshape([1,2,3,4],[2,2]) ; base = expo
call pow(base, expo, res)
print*,res


contains
   elemental subroutine pow ( base, expo, res )
   integer, intent(in) :: base, expo
   integer, intent(in out) :: res
   res = base ** expo
   end subroutine pow

end
