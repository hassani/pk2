! For compiling (or use the makefile):

! gfortran -fcheck=all -fbacktrace -Wall -fimplicit-none -Og -I ../../mod/gfortran solvlin.f90 -lblas -llapack -L../../lib/gfortran -lpk2 -o solvlin

! ifort -check all -traceback -gen-interfaces -warn interfaces -O0 -fpp -I ../../mod/ifort solvlin.f90 -lblas -llapack -L../../lib/ifort -lpk2 -o solvlin

! nagfor -C=all -O0 -fpp -kind=byte -I ../../mod/nagfor solvlin.f90  -lblas -llapack -L../../lib/nagfor -lpk2 -o solvlin

program Check_Util_Other
   use pk2mod_m
   
   implicit none
   
!---------------------------------------------------------------------------      
   type(err_t) :: stat ! for error/warning report      
!--------------------------------------------------------------------------- 

   call signalHandler_SignalCatch (unit = STDOUT, title = '--> Check_Util_Other:')
!   
!- disable automatic stop when an error occurs:
!
   call err_SetHaltingMode ( halting = .false. )
   
   
   call test_util_SolvLin
   
   call stat%Destroy()
   
contains

!=============================================================================================
   SUBROUTINE test_util_SolvLin
!=============================================================================================

   real(Rkind), allocatable :: Ac(:,:), Bc(:,:), Xc(:,:)
   real   (Rkind)              :: rcond = 1e-2
   integer :: i 
   
   allocate(Ac(2,4), Bc(2,1))
   
!    Ac = reshape([( 0.47,-0.34), (-0.32,-0.23), ( 0.35,-0.60), ( 0.89, 0.71), (-0.19, 0.06),  &
!                  (-0.40, 0.54), (-0.05, 0.20), (-0.52,-0.34), (-0.45,-0.45), ( 0.11,-0.85),  &
!                  ( 0.60, 0.01), (-0.26,-0.44), ( 0.87,-0.11), (-0.02,-0.57), ( 1.44, 0.80),  &
!                  ( 0.80,-1.02), (-0.43, 0.17), (-0.34,-0.09), ( 1.14,-0.78), ( 0.07, 1.14)], &
!                  [5,4])
!       
!    Bc = reshape([(-1.08,-2.59), (-2.61,-1.49), ( 3.13,-3.61), ( 7.33,-8.01), ( 9.12, 7.63)], &
!                  [5,1])  
                 
   
   Ac(1,:)=[1,2,3,4]; Ac(2,:)=[5,6,7,8];  Bc(:,1)=[10,26]
       
   print*
   print*,'************* Begin test_util_SolvLin *************'
   print*
   
   call util_SolvLinR1 ( Ac, Bc, rcond, stat )

   print*,'X=',Bc
   call stat%display() 

   
   print*
   print*,'-------------  End test_util_SolvLin  -------------'  
   print*
         
   END SUBROUTINE test_util_SolvLin
   


                        
end program Check_Util_Other
   
   
