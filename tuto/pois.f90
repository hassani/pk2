program poisson2d
!- Example using the pk2 module to solve by FDM the 2d Poisson problem:
!
!     -laplacian(u) = f,   in  Omega = (0,1)x(0,1)
!                u  = g,   on  dOmega
!
!  We take here:  f(x,y) = (w1^2 + w2^2) * sin(w1*x) * cos(w2*y)
!                 g(x,y) = sin(w1*x) * cos(w2*y)
!
!  for which the exact solution is u(x,y) = sin(w1*x) * cos(w2*y).
!---------------------------------------------------------------------------------------------
   use pk2mod_m
   implicit none
   ! Used pk2 variables:
   type   (pk2_t)            :: K, F, U                  ! the matrix, the rhs and the solution
   type   (pk2_t)            :: XY                       ! coordinates of the grid mesh
   type   (pk2_t)            :: Zero, X, Y, Xbnod, Ybnod ! auxiliary pk2 var.
   ! Auxiliary variables of intrinsic type:
   integer(Ikind), parameter :: nx = 71, i1 = 1, i2 = 2, i4 = 4
   integer(Ikind)            :: i, bnod(4*nx), n = nx*nx ! n: total number of nodes
   real   (Rkind)            :: a = 0.0, b = 1.0, w1 = 6.28, w2 = 6.28, h
!---------------------------------------------------------------------------------------------

   Zero = 0           ! the pk2 0
   h = (b-a) / (nx-1) ! mesh size (uniform grid with hx = hy = h)

!- Grid generation:
   X = [(a+(i-1)*h, i=1,nx)] ; XY = meshgrid ([X,X]) 

!- The Matrix (penta-diagonal) of the 2d-laplacian:
   K = i4 * diag(ones(n,i1)) - diag(ones(n-i1,i1),-i1) - diag(ones(n-i1,i1),i1) &
                             - diag(ones(n-nx,i1),-nx) - diag(ones(n-nx,i1),nx)

!- The rhs: f = (w1^2 + w2^2) * sin(w1*x) * cos(w2*y):
   X = extracmat ( XY, [-i1], [i1] ) ! x coord. of the grid nodes (-1 means the whole col. 1)
   Y = extracmat ( XY, [-i1], [i2] ) ! y coord. of the grid nodes (-1 means the whole col. 2)                
   F = (h**2) * (w1**2 + w2**2) * sin(w1*X) .m. cos(w2*Y) ! note: ".m.": element-wise mult.

!- Insert the BC (adapt K and F):
!  boundary node #s:
   bnod = [ ( i, n-nx+i, (i-1)*nx + 1, i*nx, i=1,nx ) ]

!  boundary node coordinates:  
   Xbnod = extracmat ( XY, bnod, [i1] ) ! elements of the 1st column of XY
   Ybnod = extracmat ( XY, bnod, [i2] ) ! elements of the 2nd column of XY
 
!  prescribed values on the boundary (u = g = sin(w1*x) * cos(w2*y) on dOmega):
   U =  sin(w1*Xbnod) .m. cos(w2*Ybnod)  ! (note the element-wise mult)

!  Modify K and F:    
   call F%SetSubmat ( U, bnod, [i1] )              ! put U(Xbnod,Ybnod) on elt. #bnod(:) of F 
   call K%SetSubmat ( Zero, bnod, [(i,i=1,n)] )    ! put 0 on the rows #bnod(:) of K
   call K%SetSubmat ( eye(4*nx,4*nx), bnod, bnod ) ! put 1 on the diagonal elt. #bnod(:) of K

!- Solve the system (use the operator .bslah. of the function mldivide of pk2f):
   U = K .bslash. F  ! or  U = mldivide (K, F)
   
!- Print the result (in the form: X,Y,U,Uexact), using for example the pk2_writemat routine:
   call pk2_writemat &
        (                                                     &
        mergemats([XY,U,sin(w1*X) .m. cos(w2*Y)],delim=',,'), & ! the pk2 object to print
        fname = 'out_poisson2d',                              & ! name of the file
        size = .false.,                                       & ! do not print the dimensions (optional)
        format = '(4e13.5)',                                  & ! selected format (optional)
        title =                                               & ! set a header (optional)
        'Solution of -lap(u) = f, u = g on boundary \n'  //   & ! note: '\n' for new line
        'x            y            u            u_exact'      &
        )
end program poisson2d